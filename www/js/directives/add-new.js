var FORM_TPL_CONTENT =
	'<form name="newThingForm" ng-submit="doAddThing(newThing);" novalidate>' +
	'	<label class="item item-input">' +
	'		<i class="ion-plus"></i>' +
	'		<input type="text" id="newthing-title" placeholder="List item" ng-model="newThing.title" required>' +
	'	</label>' +
    '   <input type="submit" style="position: absolute; left: -9999px; width: 1px; height: 1px;"/>' +
	'</form>';

/**
* @ngdoc directive
* @name ptlNew
* @restrict E
* @description
*
* @usage
*
* ```html
* <ptl-new></ptl-new>
* ```
*/
ptlDirectives
.directive('ptlNew', [
  '$compile',
function($compile) {
  return {
    restrict: 'E',
    controller: 'ThingsCtrl',
    compile: function(element, $attrs) {
		var innerElement = FORM_TPL_CONTENT;
		element.append(innerElement);
        var input = element.find('input')[0];
		
		return function link($scope, element, $attrs) {
			$scope.$watch('inputDirty', function(newValue, oldValue) {
                if (newValue) {
                    input.blur();
                    input.focus();
                    $scope.inputDirty = false;
                }
            });			
		};
    }
  };
}]);