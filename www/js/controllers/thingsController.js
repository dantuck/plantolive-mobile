ptlControllers

.controller('ThingsCtrl', ['$scope', '$ionicModal', '$ionicListDelegate', 'thingsService', 'localStorageService', '$stateParams', '$state'
   ,
    function ($scope, $ionicModal, $ionicListDelegate, thingsService, localStorageService, $stateParams, $state) {
        $scope.newThing = {};
	   
        $scope.setThing = function (thing) {
            if (!thing) {
                thing = {id: ''};
            }

            $scope.thing = thing;
            $scope.things = thingsService.get(thing.id);

            $scope.viewTitle = thing.title || 'PlantoLive';
        };

        $scope.gotToThing = function (thing) {
            thingsService.thing = thing;
            $state.go('app.single', {thingId: thing.id});
        };

        if ($stateParams.thingId) {
            $scope.setThing(thingsService.thing);
        } else {
            $scope.setThing();
        }

        $ionicModal.fromTemplateUrl('templates/thing/add.html', {
            scope: $scope   
        }).then(function (modal) {
           $scope.addThingModal = modal;
        });

        $scope.$on('modal.hidden', function() {
            $scope.addThingModal.scope.thing = {};
        });

        $scope.closeAddThing = function () {
            $scope.addThingModal.hide();
        };

        $scope.addThing = function () {
            $scope.addThingModal.scope.thing = {};
            $scope.addThingModal.focusFirstInput = true;

            $scope.addThingModal.show();  
        };

        $scope.doAddThing = function (thing) {
            if(thing.title.length > 0) {
                $scope.things.open.push(thingsService.put(thing, $scope.things._, $scope.thing.id));
                $scope.inputDirty = true;
                $scope.newThing = {};
            }
        };

        $scope.doRemoveThing = function (thing) {
            $ionicListDelegate.closeOptionButtons();
    //        _.remove($scope.things, function(currentObject) {
    //            return currentObject === thing;
    //        });

            thing.deleted = true;

            var index = $scope.things._.indexOf(thing);
            $scope.things._[index] = thing;

            $scope.things = thingsService.update($scope.things._, $scope.thing.id)
        };

        $scope.toggleCompleted = function (thing) {
            typeof thing.checked === 'undefined' && (thing.checked = false);

            thing.checked = !thing.checked;

            var index = $scope.things._.indexOf(thing);
            $scope.things._[index] = thing;

            $scope.things = thingsService.update($scope.things._, $scope.thing.id);
        };

}])

;