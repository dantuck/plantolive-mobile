ptlControllers

.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', '$state', 'thingsService', '$ionicPopup', function($scope, $ionicModal, $timeout, $state, thingsService, $ionicPopup) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  },

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };
	
	// A confirm dialog
	var showConfirm = function(config, actions) {
		var confirmPopup = $ionicPopup.confirm(config);
		confirmPopup.then(function(res) {
			if(res) {
				actions.confirm();
			} else {
				actions.cancel();
			}
		});
	};
	
  $scope.startOver = function () {
	  showConfirm(
		  {
		  	'title': 'Reset',
			  'template': 'Are you sure you want to reset the application?'
		  },
		  {
			  'confirm': function() {
				thingsService.clearAll();
				$state.go($state.current, {}, {reload: true});
			  },
			  'cancel': function () {}
		  }
	  );
  };
	
  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
}])