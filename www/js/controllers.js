////ptlControllers
////
////.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', '$state', 'thingsService', '$ionicPopup', function($scope, $ionicModal, $timeout, $state, thingsService, $ionicPopup) {
////  // Form data for the login modal
////  $scope.loginData = {};
////
////  // Create the login modal that we will use later
////  $ionicModal.fromTemplateUrl('templates/login.html', {
////    scope: $scope
////  }).then(function(modal) {
////    $scope.modal = modal;
////  });
////
////  // Triggered in the login modal to close it
////  $scope.closeLogin = function() {
////    $scope.modal.hide();
////  },
////
////  // Open the login modal
////  $scope.login = function() {
////    $scope.modal.show();
////  };
////	
////	// A confirm dialog
////	var showConfirm = function(config, actions) {
////		var confirmPopup = $ionicPopup.confirm(config);
////		confirmPopup.then(function(res) {
////			if(res) {
////				actions.confirm();
////			} else {
////				actions.cancel();
////			}
////		});
////	};
////	
////  $scope.startOver = function () {
////	  showConfirm(
////		  {
////		  	'title': 'Reset',
////			  'template': 'Are you sure you want to reset the application?'
////		  },
////		  {
////			  'confirm': function() {
////				thingsService.clearAll();
////				$state.go($state.current, {}, {reload: true});
////			  },
////			  'cancel': function () {}
////		  }
////	  );
////  };
//	
//  // Perform the login action when the user submits the login form
//  $scope.doLogin = function() {
//    console.log('Doing login', $scope.loginData);
//
//    // Simulate a login delay. Remove this and replace with your login
//    // code if using a login system
//    $timeout(function() {
//      $scope.closeLogin();
//    }, 1000);
//  };
//}])
//
//.controller('ThingsCtrl', ['$scope', '$ionicModal', '$ionicListDelegate', 'thingsService', 'localStorageService', '$stateParams', '$state'
//   , function($scope, $ionicModal, $ionicListDelegate, thingsService, localStorageService, $stateParams, $state) {
//	   
//	$scope.newThing = {};
//	   
//    $scope.setThing = function (thing) {
//        if (!thing) {
//            thing = {id: ''};
//        }
//        
//		$scope.thing = thing;
//        $scope.things = thingsService.get(thing.id);
//		
//        $scope.viewTitle = thing.title || 'Plan';
//    };
//       
//    $scope.gotToThing = function (thing) {
//        thingsService.thing = thing;
//        $state.go('app.single', {thingId: thing.id});
//    };
//       
//    if ($stateParams.thingId) {
//        $scope.setThing(thingsService.thing);
//    } else {
//        $scope.setThing();
//    }
//	   
//    $ionicModal.fromTemplateUrl('templates/add-thing.html', {
//        scope: $scope   
//    }).then(function (modal) {
//       $scope.addThingModal = modal;
//    });
//    
//    $scope.$on('modal.hidden', function() {
//        $scope.addThingModal.scope.thing = {};
//    });
//    
//    $scope.closeAddThing = function () {
//        $scope.addThingModal.hide();
//    };
//    
//    $scope.addThing = function () {
//		$scope.addThingModal.scope.thing = {};
//		$scope.addThingModal.focusFirstInput = true;
//		
//        $scope.addThingModal.show();  
//    };
//    
//    $scope.doAddThing = function (thing) {
//		$scope.things.open.push(thingsService.put(thing, $scope.things._, $scope.thing.id));
//		$scope.newThing = {};
//		$scope.newThingForm.$setPristine();
//		$scope.newThingForm.$setUntouched();
//		
//		
////        $scope.closeAddThing();
//    };
//    
//    $scope.doRemoveThing = function (thing) {
//        $ionicListDelegate.closeOptionButtons();
////        _.remove($scope.things, function(currentObject) {
////            return currentObject === thing;
////        });
//		
//		thing.deleted = true;
//		
//		var index = $scope.things._.indexOf(thing);
//		$scope.things._[index] = thing;
//		
//		$scope.things = thingsService.update($scope.things._, $scope.thing.id)
//    };
//	   
//	$scope.toggleCompleted = function (thing) {
//		typeof thing.checked === 'undefined' && (thing.checked = false);
//		
//		thing.checked = !thing.checked;
//		
//		var index = $scope.things._.indexOf(thing);
//		$scope.things._[index] = thing;
//		
//		$scope.things = thingsService.update($scope.things._, $scope.thing.id);
//	};
//    
//}])