ptlServices

.factory('thingsService', ['localStorageService', function (localStorageService) {
    var nextUnique = function (groupId) {
        var key = groupId ? groupId + '_' : '';
        return _.uniqueId(key);
    };
	
	var split = function (things) {
	   return {
		   _: things,
		   checked: _.where(things, { 'checked': true, 'deleted': false || undefined }),
		   open: _.where(things, { 'checked': false, 'deleted': false || undefined })
	   };
	};
    
    var thingsService = {
		
        nextUnique: function (groupId) {
            return nextUnique(groupId);
        },
        
        get: function (id) {
            return split(localStorageService.get('things_' + id) || []);
        },
        
        put: function (thing, things, id) {
            if (!thing || !things)
                return;
            
            id = id ? id : '';
            
            thing.id = _.uuidSafe();
			thing.checked = false;
			
			things.push(thing);
			localStorageService.set('things_' + id, things);
            return thing;
        },
		
		update: function (things, id) {
			localStorageService.set('things_' + id, things);
			return split(things);
		},
		
		toggleActive: function (things, thing, groupId) {
			if (!thing || !things)
                return;
		},
		
		clearAll: function () {
			localStorageService.clearAll();
		},
        
    };
    
    return thingsService;
}])

.factory('batchLog', ['$interval', '$log', function($interval, $log) {
  var messageQueue = [];

  function log() {
    if (messageQueue.length) {
      $log.log('batchLog messages: ', messageQueue);
      messageQueue = [];
    }
  }

  // start periodic checking
  $interval(log, 50000);

  return function(message) {
    messageQueue.push(message);
  }
}]);
